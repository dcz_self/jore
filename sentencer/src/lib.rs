/*! Puts together fixed size sentences coming from a fixed size buffer and exposes each sentence as continuous bytes using a fixed size buffer. */

#![no_std]
use core::cell::Cell;
use core::cmp;
use core::iter;

/// Buffer too long, split it up.
#[derive(Debug, PartialEq)]
pub enum Error {
    TooLong,
    WrongState,
}

#[derive(Debug, Clone, Copy)]
enum State {
    Accepting(u8),
    // keeps index
    Producing(u8),
}

#[derive(Debug, Clone, Copy)]
enum End {
    /// Can finish storing one message before needing to wrap.
    /// Message starts at start.
    Continuous(u8),
    /// from start to back, the message is complete,
    /// new message from 0 to front
    Wrapped{ back: u8, front: u8 },
}

const BUFSIZE: usize = 64*3;

/// HAHA it's broken :(
/// At least when messages can be corrupted
pub struct NmeaSentencer {
    buf: [u8; BUFSIZE], // two sentences' worth of storage avoids copying, 3 buffers also
    // Sentences are always continuous in the buffer.
    // During parsing, an unfinished sentence is either ended by front_end or mid_end.
    //
    // |front_end,start....mid_end___|
    //
    // |front_end___start...\r\n....mid_end____|
    //
    // |...\r\n...front_end____start....\r\nmid_end___|
    /// Invariant: if mid_end is above safe size, it's because it holds a finished sentence.
    end: End,
    // contains start
    // some way to force accepting only after iteration is done
    state: Cell<State>,
}

impl NmeaSentencer {
    pub fn new() -> Self {
        Self {
            buf: [0; BUFSIZE],
            end: End::Continuous(0),
            state: Cell::new(State::Accepting(0)),
        }
    }
    
    fn dump(&self) {/*
        let start = match self.state.get() {
            State::Accepting(start) | State::Producing(start) => start as usize,
        };
        match self.end {
            End::Continuous(end) => {
                dbg!(
                    "mid",
                    std::str::from_utf8(&self.buf[start..(end as usize)]
                ).unwrap());
            },
            End::Wrapped { front, back } => {
                let front = front as usize;
                let back = back as usize;
                let start = if start > front {
                    dbg!(
                        "back",
                        std::str::from_utf8(&self.buf[start..back]),
                    );
                    0
                } else { start };

                dbg!(
                    "front",
                    std::str::from_utf8(&self.buf[start..front]),
                );
            },
        };
        dbg!("SEN", self.end, &self.state);*/
    }

    /// Add a buffer of max 64 bytes.
    /// Afterwards, `read` must be called.
    // Invariant: will never leave mid_end above safe size
    pub fn add(&mut self, bytes: &[u8])
        -> Result<(), Error>
    {
        if let State::Accepting(start) = self.state.get() {
            if bytes.len() > 64 {
                Err(Error::TooLong)
            } else {
                // Reading cannot adjust end, so do it now
                let end = if let End::Wrapped { front, .. } = self.end {
                    // All data between start and back have already been given
                    if start <= front { End::Continuous(front) }
                    else { self.end }
                } else {
                    self.end
                };
                const SAFESIZE: usize = BUFSIZE - 82;
                let ret = match end {
                    End::Wrapped { front, back } => {
                        let front = front as usize;
                        // start is supposed to be far away in the front now
                        self.buf[(front)..][..bytes.len()].copy_from_slice(bytes);
                        self.end = End::Wrapped {
                            front: (front + bytes.len()) as u8,
                            back,
                        };
                        Ok(())
                    },
                    End::Continuous(back) => {
                        let mid_end = back as usize;

                        let free = SAFESIZE.saturating_sub(mid_end);
                        let to_copy = cmp::min(free, bytes.len());
                        self.buf[mid_end..][..to_copy].copy_from_slice(&bytes[..to_copy]);
                        let mid_end = mid_end + to_copy;
                        let bytes = &bytes[to_copy..];
                        
                        // copy until sentence end
                        
                        enum ParseState {
                            Inside,
                            CR,
                            LF,
                        }

                        let mut state = if mid_end == start as usize {
                            ParseState::Inside
                        } else if self.buf[mid_end - 1] == b'\r' {
                            ParseState::CR
                        } else {
                            ParseState::Inside
                        };
                        let mut copied = 0;
                        let mut found = false;
                        for i in 0..(bytes.len()) {
                            self.buf[mid_end..][i] = bytes[i];
                            state = match (state, bytes[i]) {
                                (_, b'\r') => ParseState::CR,
                                (ParseState::CR, b'\n') => ParseState::LF,
                                _ => ParseState::Inside,
                            };
                            copied = i + 1;
                            if let ParseState::LF = state {
                                found = true;
                                break;
                            }
                        }
                        let end = (mid_end + copied) as u8;
                        self.end = if found {
                            End::Wrapped {
                                back: end,
                                front: 0,
                            }
                        } else {
                            End::Continuous(end)
                        };
                        
                        if bytes[copied..].len() > 0 {
                            self.add(&bytes[copied..])
                        } else {
                            Ok(())
                        }
                    },
                };
                self.state.set(State::Producing(start));
                ret
            }
        } else {
            Err(Error::WrongState)
        }
    }
    
    /// Return messages.
    /// Must be emptied before `add` can be called
    pub fn read<'a>(&'a self) -> Result<impl Iterator<Item=&'a [u8]> + 'a, Error> {
        if let State::Producing(_) = self.state.get() {
            Ok(iter::from_fn(move || {
                if let State::Producing(start) = self.state.get() {
                    let start = start as usize;
                    let (start, end) = match self.end {
                        End::Continuous(end) => (start, end as usize),
                        End::Wrapped { front, back } => {
                            let front = front as usize;
                            let start =
                                if start == back as usize { 0 }
                                else { start };
                            (
                                start,
                                if start <= front { front }
                                else { back as usize }
                            )
                        },
                    };
                    
                    let found = self.buf[start..end]
                        .windows(2)
                        .position(|x| x == b"\r\n");
                    // Dropped messages. Give up.
                    // This can be done more eagerly but who cares
                    let found = match found {
                        Some(found) => Some(cmp::min(found, start + 80)),
                        None => if start + 80 < end {
                            Some(end)
                        } else {
                            None
                        }
                    };
                    let (new_state, ret) = if let Some(position) = found {
                        let position = position + 2;
                        let found_end = start + position;
                        (
                            State::Producing(found_end as u8),
                            Some(&self.buf[start..found_end]),
                        )
                    } else {
                        (State::Accepting(start as u8), None)
                    };
                    self.state.set(new_state);
                    ret
                } else {
                    None
                }
            }))
        } else {
            Err(Error::WrongState)
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum CopyState {
    Adding,
    Processing,
}
/// I give up being clever. Copying message ends should be much simpler.
pub struct CopySentencer {
    pub buf: [u8; 82*2],
    pub last_start: u8,
    pub end: u8,
    state: Cell<CopyState>,
}

impl CopySentencer {
    pub fn new() -> Self  {
        Self {
            buf: [0; 82*2],
            last_start: 0,
            end: 0,
            state: Cell::new(CopyState::Adding),
        }
    }
    
    pub fn add(&mut self, bytes: &[u8]) -> Result<(), Error> {
        if let CopyState::Adding = self.state.get() {
            if bytes.len() > 82 {
                Err(Error::TooLong)
            } else {
                // Shift consumed stuff back to front,
                // so that only an incomplete message start is saved,
                // if anything.
                for src in self.last_start..self.end {
                    let src = src as usize;
                    let dst = src - self.last_start as usize;
                    self.buf[dst] = self.buf[src];
                }
                self.end -= self.last_start;
                self.last_start = 0;
                
                // Drop broken messages
                if self.end >= 82 {
                    self.end = 0;
                }
                
                // Add new bytes
            
                let end = self.end as usize;
                let mut last_start = self.last_start;
                
                enum ParseState {
                    Inside,
                    CR,
                    LF,
                }

                let mut state = if end == 0 {
                    ParseState::Inside
                } else if self.buf[end - 1] == b'\r' {
                    ParseState::CR
                } else {
                    ParseState::Inside
                };
                
                for i in 0..(bytes.len()) {
                    self.buf[end..][i] = bytes[i];
                    
                    state = match (state, bytes[i]) {
                        (_, b'\r') => ParseState::CR,
                        (ParseState::CR, b'\n') => ParseState::LF,
                        _ => ParseState::Inside,
                    };
                    if let ParseState::LF = state {
                        last_start = (end + i + 1) as u8;
                    }
                }
                
                self.last_start = last_start;
                self.end += bytes.len() as u8;
                self.state.set(CopyState::Processing);
                Ok(())
            }
        } else {
            Err(Error::WrongState)
        }
    }
    
    pub fn read<'a>(&'a self) -> Result<impl Iterator<Item=&'a [u8]> + 'a, Error> {
        if let CopyState::Processing = self.state.get() {
            let mut last_found = 0;
            Ok(iter::from_fn(move || {
                let found = &self.buf[last_found..self.end as usize]
                    .windows(2)
                    .position(|x| x == b"\r\n");
                if let Some(position) = found {
                    let start = last_found;
                    let new_found = last_found + position + 2;
                    last_found = new_found;
                    Some(&self.buf[start..new_found])
                } else {
                    self.state.set(CopyState::Adding);
                    None
                }
            }))
        } else {
            Err(Error::WrongState)
        }
    }
    fn dump(&self) {
        //dbg!(self.last_start, self.end, &self.state);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use CopySentencer as NmeaSentencer;
    use assert_matches::assert_matches;
    #[test]
    fn empty() {
        let mut s = NmeaSentencer::new();
        s.add(b"").unwrap();
        let mut r = s.read().unwrap();
        assert_eq!(r.next(), None);
    }
    
    #[test]
    fn short() {
        let mut s = NmeaSentencer::new();
        assert_eq!(s.read().map(|_| ()), Err(Error::WrongState));
        s.add(b"test\r\n").unwrap();
        let mut r = s.read().unwrap();
        assert_eq!(r.next(), Some(b"test\r\n".as_slice()));
        //assert_eq!(s.add(b"test\r\n"), Err(Error::WrongState));
        assert_eq!(r.next(), None);
        assert_eq!(r.next(), None);
    }
    
    #[test]
    fn zero() {
        let mut s = NmeaSentencer::new();
        s.add(b"test").unwrap();
        let mut r = s.read().unwrap();
        assert_eq!(r.next(), None);
    }
    
    #[test]
    fn two() {
        let mut s = NmeaSentencer::new();
        s.add(b"test\r\n \r\n").unwrap();
        let mut r = s.read().unwrap();
        assert_eq!(r.next(), Some(b"test\r\n".as_slice()));
        assert_eq!(r.next(), Some(b" \r\n".as_slice()));
        assert_eq!(r.next(), None);
    }
    #[test]
    fn twotimes() {
        let mut s = NmeaSentencer::new();
        s.add(b"test\r\n").unwrap();
                s.dump();
        {
            let mut r = s.read().unwrap();
            assert_eq!(r.next(), Some(b"test\r\n".as_slice()));
            assert_eq!(r.next(), None);
        }
        s.add(b" \r\n").unwrap();
                s.dump();
        let mut r = s.read().unwrap();
        assert_eq!(r.next(), Some(b" \r\n".as_slice()));
        assert_eq!(r.next(), None);
    }
    
    #[test]
    fn split() {
        let mut s = NmeaSentencer::new();
        s.add(b"test").unwrap();
        {
            let mut r = s.read().unwrap();
            assert_eq!(r.next(), None);
        }
        s.add(b" \r\n").unwrap();
        let mut r = s.read().unwrap();
        assert_eq!(r.next(), Some(b"test \r\n".as_slice()));
        assert_eq!(r.next(), None);
    }
    
    #[test]
    fn splitin() {
        let mut s = NmeaSentencer::new();
        s.add(b"test\r").unwrap();
        {
            let mut r = s.read().unwrap();
            assert_eq!(r.next(), None);
        }
        s.add(b"\n").unwrap();
        let mut r = s.read().unwrap();
        assert_eq!(r.next(), Some(b"test\r\n".as_slice()));
        assert_eq!(r.next(), None);
    }
    
    #[test]
    fn long() {
        let mut s = NmeaSentencer::new();
        s.add(&[b'x'; 64][..]).unwrap();
        {
            let mut r = s.read().unwrap();
            assert_eq!(r.next(), None);
        }
        s.add(b"\r\n").unwrap();
        let mut r = s.read().unwrap();
        assert_matches!(r.next(), Some(f) if f.len() == 66);
        assert_eq!(r.next(), None);
    }
    #[test]
    fn twobig() {
        let mut s = NmeaSentencer::new();
        s.add(&[b'x'; 64][..]).unwrap();
        {
            let mut r = s.read().unwrap();
            assert_eq!(r.next(), None);
        }
        s.add(b"\r\n").unwrap();
        {
            let mut r = s.read().unwrap();
            assert_matches!(r.next(), Some(f) if f.len() == 66);
            assert_eq!(r.next(), None);
        }
        s.add(&[b'x'; 63][..]).unwrap();
        {
            let mut r = s.read().unwrap();
            assert_eq!(r.next(), None);
        }
        s.add(b"\r\n").unwrap();
        let mut r = s.read().unwrap();
        assert_matches!(r.next(), Some(f) if f.len() == 65);
        assert_eq!(r.next(), None);
    }
    
    #[test]
    fn many() {
        let mut s = NmeaSentencer::new();
        for i in 0..5 {
            s.add(&[b'x'; 64][..]).unwrap();
            s.dump();
            {
                let mut r = s.read().unwrap();
                assert_eq!(r.next(), None);
            }
            s.add(b"\r\n").unwrap();
            s.dump();
            {
                let mut r = s.read().unwrap();
                assert_matches!(r.next(), Some(f) if f.len() == 66);
                assert_eq!(r.next(), None);
            }
        }
    }
    
    #[test]
    fn real() {
        let mut s = NmeaSentencer::new();
        s.add(b"3.948,V,M*6A\r\n$GNGSA,A,1,,,,,,,,,,,,,25.5,25.5,25.5,1*01\r\n$GNGSA").unwrap();
        {
            let mut r = s.read().unwrap();
            assert_eq!(r.next(), Some(&b"3.948,V,M*6A\r\n"[..]));
            assert_eq!(r.next(), Some(&b"$GNGSA,A,1,,,,,,,,,,,,,25.5,25.5,25.5,1*01\r\n"[..]));
            assert_eq!(r.next(), None);
        }
        s.add(b"\r\n").unwrap();
        {
            let mut r = s.read().unwrap();
            assert_matches!(r.next(), Some(f) if f.len() == 66);
            assert_eq!(r.next(), None);
        }
        s.add(&[b'x'; 63][..]).unwrap();
        {
            let mut r = s.read().unwrap();
            assert_eq!(r.next(), None);
        }
        s.add(b"\r\n").unwrap();
        let mut r = s.read().unwrap();
        assert_matches!(r.next(), Some(f) if f.len() == 65);
        assert_eq!(r.next(), None);
        
    }
}
