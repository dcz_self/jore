#![no_std]

use embedded_graphics_core as egc;
use egc::Pixel;
use egc::prelude::{ OriginDimensions, PixelColor, Size };
use libtock2::screen;

#[derive(PartialEq, Clone, Copy)]
pub enum Monochrome {
    Dark,
    Light,
}

impl PixelColor for Monochrome {
    type Raw = egc::pixelcolor::raw::RawU1;
}

pub struct MonoScreen {
    pub buffer: [u8; 4096],
}

impl MonoScreen {
    pub fn flush(&self) -> Result<(), libtock2::platform::ErrorCode> {
        screen::Screen::write(&self.buffer)
    }
}

impl egc::draw_target::DrawTarget for MonoScreen {
    type Color = Monochrome;
    type Error = libtock2::platform::ErrorCode;

    fn draw_iter<I>(&mut self, pixels: I) -> Result<(), Self::Error>
    where
        I: IntoIterator<Item = Pixel<Self::Color>>,
    {
        let w = self.size().width as usize;
        let linew = w / 8;
        for Pixel(p, c) in pixels.into_iter() {
            let byteidx = p.y as usize * linew + p.x as usize / 8;
            let bitidx = 7 - p.x as u8 % 8;
            
            fn clr(byte: u8, bit: u8) -> u8 {
                byte & !bit
            }
            
            use core::ops::BitOr;
            let op = match c {
                Monochrome::Dark => clr,
                Monochrome::Light => u8::bitor,
            };
            if byteidx < 4096 {
                self.buffer[byteidx] = op(self.buffer[byteidx], 1<< bitidx);
            }
        }
        Ok(())
    }
/*
    fn fill_contiguous<I>(
        &mut self, 
        area: &Rectangle, 
        colors: I
    ) -> Result<(), Self::Error>
    where
        I: IntoIterator<Item = Self::Color>,
    {
        let Rectangle { top_left: Point{ x, y }, size: Size { width, height } }
            = area;
        let frame = screen::Rectangle {
            x: *x as u16,
            y: *y as u16,
            width: *width as u16,
            height: *height as u16,
        };
        screen::Screen::set_frame(frame)?;
        let mut buffer = [0; 4096];
        let buffer = &mut buffer[..(width * height / 8) as usize];
        for (i, color) in colors.into_iter().enumerate() {
            let color = color as u8;
            buffer[i / 8] = buffer[i / 8] | color << (7 - i as u8 % 8);
        }
        screen::Screen::write(buffer)?;
        Ok(())
    }*/
}

impl OriginDimensions for MonoScreen {
    fn size(&self) -> Size {
        let screen::Resolution { width, height } = screen::Screen::get_resolution().unwrap();
        Size { width, height }
    }
}