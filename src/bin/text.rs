//! Prints out GNSS stream

#![no_main]
#![no_std]
use arrayvec::ArrayString;

use core::fmt::Debug; 
use core::fmt::Write;

use embedded_graphics::Drawable;
use embedded_graphics::mono_font::ascii::FONT_10X20;
use embedded_graphics::mono_font::MonoTextStyle;
use embedded_graphics::prelude::{Angle, Point, Size};
use embedded_graphics::primitives::{Arc, Circle, Line, Primitive, PrimitiveStyle, Rectangle};
use embedded_graphics::text::Text;

use jore::div_round;
use libtock_graphics::{Monochrome, MonoScreen};

use libtock2::console::Console;
use libtock2::gnss::GNSS;
use libtock2::platform::ErrorCode;
use libtock2::runtime::{set_main, stack_size};
use libtock2::screen::Screen;

use sentencer::CopySentencer;
use yanp;
use yanp::errors::NmeaSentenceError;
use yanp::parse::SentenceData;

set_main! {main}
stack_size! {0x1800}

fn main() {
    draw().unwrap()
}

fn draw() -> Result<(), ErrorCode> {
    Screen::set_power(true).unwrap();

    let buf = [0; 4096];
    let mut display = MonoScreen{ buffer: buf };

    let bg_style = PrimitiveStyle::with_fill(Monochrome::Light);
    let gauge_style = PrimitiveStyle::with_stroke(Monochrome::Dark, 20);
    let line_style = PrimitiveStyle::with_stroke(Monochrome::Dark, 1);
    let mut text_style = MonoTextStyle::new(&FONT_10X20, Monochrome::Dark);
    text_style.background_color = Some(Monochrome::Light);

    Circle::new(Point::new(72, 8), 48)
        .into_styled(line_style)
        .draw(&mut display)?;

    Line::new(Point::new(48, 16), Point::new(8, 16))
        .into_styled(line_style)
        .draw(&mut display)?;

    Text::new(
        if GNSS::driver_check() { "Hello World!" }
        else {"NO"},
        Point::new(5, 40),
        text_style,
    ).draw(&mut display)?;
    display.flush()?;
    
    let mut buf = [0; 64];

    let mut sentencer = CopySentencer::new();

    // yanp = "0.1.1" is a better API but needs manual piecing together
    let mut time = None;
    
    loop {
        let (read_count, ret) = GNSS::read(&mut buf);
        ret?;
        let raw = &buf[..read_count as usize];

        let mut outdated = false;

        sentencer.add(raw).unwrap();
        for s in sentencer.read().unwrap() {
            let result = yanp::parse_nmea_sentence(s);

            fn cw<T: Debug>(v: Option<T>, s: &'static str) {
                v.map(|v| {
                    writeln!(Console::writer(), "{} {:?}", s, v).unwrap();
                });
            }
            

            match result {
                // Ignore. This *will* happen, no reason to make it worse by stalling.
                Err(NmeaSentenceError::ChecksumError(_, _)) => {}
                Err(_e) => {
                    //writeln!(Console::writer(), "ERR {:?} {:?}", e, core::str::from_utf8(s)).unwrap();
                },
                /*
                // sat visibility
                Ok(SentenceData::GSV(s)) => {
                },
                // date time
                Ok(SentenceData::ZDA(_)) => {
                },
                */
                Ok(SentenceData::RMC(s)) => {
                    if s.time != time {
                        time = s.time;
                        if let Some(t) = time {
                            let mut digits = ArrayString::<2>::new();
                            write!(digits, "{}", t.second).unwrap();
                            Text::new(
                                &digits[..],
                                Point::new(155, 170),
                                text_style,
                            ).draw(&mut display)?;
                        }

                        outdated = true;
                    }
                    
                    if let Some((s, f)) = s.speed_knots {
                        let speed_knots_hundredths = s as u32 * 100 + f as u32;
                        let speed_dmh = speed_knots_hundredths * 1852 / 1000;
                        let speed_kmh = div_round(speed_dmh, 100);
                        writeln!(Console::writer(), "kmh {} dmh {}", speed_kmh, speed_dmh).unwrap();
                        let mut digits = ArrayString::<2>::new();
                        write!(digits, "{:02}", speed_kmh).unwrap();
                        Text::new(
                            &digits[..],
                            Point::new(5, 170),
                            text_style,
                        ).draw(&mut display)?;
                        
                        Rectangle::new(Point::new(0, 50), Size::new(196, 100))
                            .into_styled(bg_style)
                            .draw(&mut display)?;

                        Arc::new(
                            Point::new(10, 60),
                            160,
                            Angle::from_degrees(180.0),
                            Angle::from_degrees(-180.0 * speed_dmh as f32 / 5000.0),
                        ).into_styled(gauge_style)
                        .draw(&mut display)?;
        
                        outdated = true;
                    }
                    
                    cw(s.time, "time");
                    cw(s.speed_knots, "speed");
                },
                /*
                // position
                Ok(SentenceData::GLL(s)) => {
                    cw(s.time, "gltime");
                },*/
                // ground speed
                /*
                Ok(SentenceData::VTG(s)) => {
                    cw(s.speed_kmh, "speedkmh");
                    if let Some((s, _f)) = s.speed_kmh {
                        let mut digits = ArrayString::<2>::new();
                        write!(digits, "{}", s).unwrap();
                        Text::new(
                            &digits[..],
                            Point::new(5, 170),
                            text_style,
                        ).draw(&mut display)?;
        
                        outdated = true;
                    }
                },*/
                /*
                // active satellites
                Ok(SentenceData::GSA(_)) => {},
                // position again. This has more info
                Ok(SentenceData::GGA(_)) => {},
                // antenna open. yeah, whatever
                Ok(SentenceData::TXT(_)) => {},
                */
                Ok(_other) => {
                    //writeln!(Console::writer(), "Unhandled {:?}", other).unwrap();
                }
            }
        }
        if outdated {
            display.flush()?;
        }
    }
}
